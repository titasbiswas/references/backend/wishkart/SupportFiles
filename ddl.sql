-- Role: wishkart

-- DROP ROLE wishkart;

CREATE ROLE wishkart LOGIN
  ENCRYPTED PASSWORD 'md57846fb80594da3db50469c6c00cd0d34'
  SUPERUSER INHERIT CREATEDB CREATEROLE REPLICATION;

-- Database: wishkart_sales

-- DROP DATABASE wishkart_sales;

CREATE DATABASE wishkart_sales
  WITH OWNER = wishkart
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'English_United States.1252'
       LC_CTYPE = 'English_United States.1252'
       CONNECTION LIMIT = -1;


-- Table: public.kart

CREATE TABLE public.kart
(
    id bigint NOT NULL DEFAULT nextval('kart_id_seq'::regclass),
    user_id character varying COLLATE pg_catalog."default" NOT NULL,
    no_of_items integer,
    ship_addr_id bigint,
    bill_addr_id bigint,
    payment_id character varying(100) COLLATE pg_catalog."default",
    CONSTRAINT kart_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.kart
    OWNER to wishkart;

    -- Table: public.kart_items

    -- DROP TABLE public.kart_items;

    CREATE TABLE public.kart_items
    (
        id bigint NOT NULL DEFAULT nextval('kart_items_id_seq'::regclass),
        kart_id bigint NOT NULL,
        product_id character varying(255) COLLATE pg_catalog."default" NOT NULL,
        no_of_product integer NOT NULL,
        rate numeric NOT NULL,
        discount_prcntg numeric,
        line_total numeric NOT NULL,
        CONSTRAINT kart_items_pkey PRIMARY KEY (id),
        CONSTRAINT kart_item_kart_fk FOREIGN KEY (kart_id)
            REFERENCES public.kart (id) MATCH SIMPLE
            ON UPDATE NO ACTION
            ON DELETE NO ACTION
    )
    WITH (
        OIDS = FALSE
    )
    TABLESPACE pg_default;

    ALTER TABLE public.kart_items
        OWNER to wishkart;

        -- Table: public.order_details

        -- DROP TABLE public.order_details;

        CREATE TABLE public.order_details
        (
            id bigint NOT NULL DEFAULT nextval('order_id_seq'::regclass),
            order_date timestamp with time zone,
            total_amount numeric,
            ship_addr_id bigint,
            bill_addr_id bigint,
            invoice_no character varying(100) COLLATE pg_catalog."default",
            CONSTRAINT order_pkey PRIMARY KEY (id)
        )
        WITH (
            OIDS = FALSE
        )
        TABLESPACE pg_default;

        ALTER TABLE public.order_details
            OWNER to wishkart;

            -- Table: public.order_items

            -- DROP TABLE public.order_items;

            CREATE TABLE public.order_items
            (
                id bigint NOT NULL DEFAULT nextval('order_items_id_seq'::regclass),
                product_id character varying(255) COLLATE pg_catalog."default" NOT NULL,
                qty integer NOT NULL,
                rate numeric NOT NULL,
                discount_prcntg numeric,
                amount numeric NOT NULL,
                delivered_on timestamp with time zone,
                delivery_comment character varying(255) COLLATE pg_catalog."default",
                order_id bigint,
                CONSTRAINT order_items_pkey PRIMARY KEY (id),
                CONSTRAINT order_items_order_id_fkey FOREIGN KEY (order_id)
                    REFERENCES public.order_details (id) MATCH SIMPLE
                    ON UPDATE NO ACTION
                    ON DELETE NO ACTION
            )
            WITH (
                OIDS = FALSE
            )
            TABLESPACE pg_default;

            ALTER TABLE public.order_items
                OWNER to wishkart;

--------------------------------------------------------------------------------
-- Database: wishkart_customers


-- Table: public.addresses

-- DROP TABLE public.addresses;

CREATE TABLE public.addresses
(
    id bigserial NOT NULL,
    street1 character varying(255) COLLATE pg_catalog."default",
    street2 character varying(255) COLLATE pg_catalog."default",
    state character varying(100) COLLATE pg_catalog."default",
    country character varying(100) COLLATE pg_catalog."default",
    city character varying(100) COLLATE pg_catalog."default",
    postal_code character varying(20) COLLATE pg_catalog."default",
    landmark character varying(200) COLLATE pg_catalog."default",
    user_name character varying(255) COLLATE pg_catalog."default",
    phone_no character varying(20) COLLATE pg_catalog."default",
    CONSTRAINT addresses_pkey PRIMARY KEY (id),
    CONSTRAINT addresses_user_name_fkey FOREIGN KEY (user_name)
        REFERENCES public.users (user_name) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.addresses
    OWNER to wishkart;

-- Table: public.users

-- DROP TABLE public.users;

CREATE TABLE public.users
(
    id bigserial NOT NULL,
    user_name character varying(255) COLLATE pg_catalog."default" NOT NULL,
    password character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT users_pkey PRIMARY KEY (id),
    CONSTRAINT users_user_name_key UNIQUE (user_name)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.users
    OWNER to wishkart;
----------------------------------------------------------------------------------------------------------------------
-- Database: wishkart_logistics

-- DROP DATABASE wishkart_logistics;

CREATE DATABASE wishkart_logistics
  WITH OWNER = wishkart
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'English_United States.1252'
       LC_CTYPE = 'English_United States.1252'
       CONNECTION LIMIT = -1;


-- Table: outbound_shipping

-- DROP TABLE outbound_shipping;

CREATE TABLE outbound_shipping
(
  id bigserial NOT NULL,
  order_id bigint,
  item_id bigint,
  ship_addr_id bigint,
  bill_addr_id bigint,
  qty integer,
  delivered_on timestamp with time zone,
  shipped_on timestamp with time zone,
  delivery_status character varying(50),
  delivery_comment character varying(255),
  tracking_id integer,
  product_id character varying(100),
  CONSTRAINT outbound_shipping_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE outbound_shipping
  OWNER TO wishkart;
